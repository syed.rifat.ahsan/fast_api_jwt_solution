from passlib.context import CryptContext

pwd_cxt = CryptContext(schemes=["bcrypt"], deprecated="auto")


def verify_password(plain_pswd, hashed_pswd):
    return pwd_cxt.verify(plain_pswd, hashed_pswd)
