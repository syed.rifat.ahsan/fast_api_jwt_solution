from fastapi import APIRouter, Depends, status, Response, HTTPException
from urllib import response
from fastapi.security import OAuth2PasswordRequestForm

# from user_app. import user_model
from core.base_crud import BaseCRUD
from sqlalchemy.orm import Session
from . import schema, views
from core.database import get_db

# user_crud = BaseCRUD(user_model.User)
router = APIRouter(prefix="/auth", tags=["AUTH APIs"])


@router.post("/login", status_code=status.HTTP_200_OK)
def login(
    login_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)
):
    return views.authenticate_user(db, login_data)


@router.post("/refresh/access/token", status_code=status.HTTP_200_OK)
def get_refreshed_auth_token(
    refresh_token_data: schema.RefreshToken, db: Session = Depends(get_db)
):
    return views.authenticate_user(db, refresh_token_data)
