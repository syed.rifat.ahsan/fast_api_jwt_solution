from core.base_crud import BaseCRUD
from apps.user_app import models as user_model
from core.custom_decorators import handle_exception
from sqlalchemy.orm import Session
from . import schema, hashing, jwt_auth
from urllib import response
from fastapi import status, HTTPException
from datetime import timedelta
from core.settings import REFRESH_TOKEN_EXPIRE_MINUTES


user_crud = BaseCRUD(user_model.User)


@handle_exception()
def authenticate_user(db: Session, login_data):
    user = user_crud.get_by_field(
        db, "email", login_data.username, raise_exception=False
    )
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Authorizarion Failed"
        )
    if hashing.verify_password(login_data.password, user.password):
        access_token = jwt_auth.create_access_token(data={"sub": user.email})
        refresh_token = jwt_auth.create_access_token(
            data={"email": user.email, "token_type": "refresh"},
            expires_delta=timedelta(minutes=REFRESH_TOKEN_EXPIRE_MINUTES),
        )
        response.status_code = status.HTTP_200_OK
        return {
            "status": True,
            "user_id": user.id,
            "access_token": access_token,
            "refresh_token": refresh_token,
        }
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Authorizarion Failed"
        )
