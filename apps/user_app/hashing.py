from passlib.context import CryptContext

pwd_cxt = CryptContext(schemes=["bcrypt"], deprecated="auto")


def hash_password(pswd):
    return pwd_cxt.hash(pswd)


def verify_password(plain_pswd, hashed_pswd):
    return pwd_cxt.verify(plain_pswd, hashed_pswd)
