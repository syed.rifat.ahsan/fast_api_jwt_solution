from fastapi import APIRouter, Depends, status, Response, HTTPException
from . import schema
from core.database import *
from sqlalchemy.orm import Session
from typing import Optional
from .views import *
from core.custom_decorators import handle_exception
from core.base_crud import BaseCRUD
from . import models
import ipdb
from apps.auth_app.jwt_auth import get_current_user
from apps.user_app import schema as user_schema

router = APIRouter(prefix="/user", tags=["User APIs"])
user_crud = BaseCRUD(models.User)


@router.get("/", response_model=schema.UserViewList)
def get_all_users_list(
    db: Session = Depends(get_db),
    get_current_user: user_schema.User = Depends(get_current_user),
):
    return schema.UserViewList(status=True, user_list=user_crud.get_all(db))


@router.delete("/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_an_user(id, db: Session = Depends(get_db)):
    return delete_user(id, db)


@router.get(
    "/{id}",
    status_code=status.HTTP_200_OK,
    response_model=schema.UserView,
)
def get_a_single_user(
    id: int,
    response: Response,
    qry: Optional[str] = None,
    db: Session = Depends(get_db),
):
    return user_crud.get(db, id)
    # return {"data": user}


@router.put("/{id}", status_code=status.HTTP_202_ACCEPTED)
def update_an_user(id, request_body: schema.User, db: Session = Depends(get_db)):
    try:
        print("request_body", request_body)
        return user_crud.update_full(db, id, request_body)
    except Exception as e:
        # response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        # return {"status": False, "message": str(e)}
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail={"status": False, "message": str(e)},
        )


@router.patch("/{id}", status_code=status.HTTP_200_OK)
def partial_update_user(
    id, request_body: schema.UserUpdate, db: Session = Depends(get_db)
):
    return user_crud.update_partial(db, id, request_body)


@router.post("/create", status_code=status.HTTP_201_CREATED)
def create_user(request_body: schema.User, db: Session = Depends(get_db)):
    return user_crud.create(db, request_body)
