from fastapi import APIRouter, Depends, HTTPException, status, Response
from . import schema
from . import models
import apps.user_app.models as user_models
from core.database import *
from sqlalchemy.orm import Session
from urllib import response
from typing import Optional
from . import views
from core.base_crud import BaseCRUD

# from oauth2 import get_current_user
from sqlalchemy.sql import exists

router = APIRouter(prefix="/item", tags=["Items APIs"])
item_crud = BaseCRUD(models.Item)


@router.get("/", status_code=status.HTTP_200_OK)
# , response_model=schema.ItemList
def get_all_item(db: Session = Depends(get_db)):
    try:
        items = item_crud.join(db, user_models.User)
        # return schema.ItemList(items=items)
        return items
    except Exception as e:
        raise e


@router.get("/{id}", status_code=status.HTTP_200_OK, response_model=schema.ItemView)
def get_an_item(id, db: Session = Depends(get_db)):
    try:
        item = item_crud.get(db, id)
        print("Item", item)
        return item
    except Exception as e:
        raise e
    # return item_crud.get_item(db, id)


@router.post(
    "/create",
    status_code=status.HTTP_201_CREATED,
    response_model=schema.ItemView,
)
def create_item(
    request: schema.Item,
    db: Session = Depends(get_db),
    # get_current_user: schema.User = Depends(get_current_user),
):
    if not db.query(exists().where(user_models.User.id == request.owner_id)).scalar():
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail={
                "status": False,
                "message": f"No user found with id {request.owner_id}",
            },
        )
    # return item_crud.get_all(db)
    return item_crud.create(request, db)
