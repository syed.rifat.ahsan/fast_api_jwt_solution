from .database import Base
from typing import TypeVar, List, Any
from sqlalchemy.orm import Session
from fastapi import HTTPException, status
from sqlalchemy import update, delete

Model = TypeVar("Model", bound=Base)


class BaseCRUD:
    def __init__(self, model: Model) -> None:
        self.model = model

    def raise_no_object_found_error(self, field_name, field_val) -> None:

        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail={
                "status": False,
                "message": f"No {self.model.__name__} found with {field_name} --> {field_val}",
            },
        )

    def get(self, db: Session, id: int, raise_exception=True) -> Model:
        model_obj = db.query(self.model).filter(self.model.id == id).first()
        if not model_obj:
            if raise_exception:
                self.raise_no_object_found_error("id", id)
            else:
                return None
        return model_obj

    def get_by_field(
        self, db: Session, field_name: str, field_val: Any, raise_exception=True
    ):
        if type(field_val) == str:
            field_val = f'"{field_val}"'
        filter_str = f"self.model.{field_name}=={field_val}"
        model_obj = db.query(self.model).filter(eval(filter_str)).first()
        if not model_obj:
            if raise_exception:
                self.raise_no_object_found_error(field_name, field_val)
            else:
                return None
        return model_obj

    def create(self, db: Session, post_data: Model) -> Model:
        new_obj = self.model(**post_data.__dict__)
        db.add(new_obj)
        db.commit()
        db.refresh(new_obj)
        return new_obj

    def delete(self, db: Session, post_data: Model) -> Model:
        db.execute(delete(self.model).where(self.model.id == id))
        db.commit()
        return {
            "status": True,
            "message": f"{self.model.__name__} {id} updated successfully",
        }

    def soft_delete(self, db: Session, post_data: Model) -> Model:
        db.execute(
            update(self.model).where(self.model.id == id).values(is_deleted=True)
        )
        db.commit()
        return {
            "status": True,
            "message": f"{self.model.__name__} {id} updated successfully",
        }

    def update_full(
        self, db: Session, id: int, post_data: Model, raise_exception=True
    ) -> Model:
        model_obj = db.query(self.model).filter(self.model.id == id)
        if not model_obj.first():
            if raise_exception:
                self.raise_no_object_found_error("id", id)
            else:
                return None
        model_obj.update(
            post_data.__dict__,
            synchronize_session=False,
        )
        db.commit()
        return {
            "status": True,
            "message": f"{self.model.__name__} {id} updated successfully",
        }

    def update_partial(self, db: Session, id: int, post_data: Model) -> Model:
        clean_data = post_data.dict(exclude_unset=True)
        db.execute(update(self.model).where(self.model.id == id).values(clean_data))

        db.commit()
        return {
            "status": True,
            "message": f"{self.model.__name__} {id} updated successfully",
        }

    def get_all(self, db: Session) -> List[Model]:
        model_obj_all = db.query(self.model).all()
        return model_obj_all

    def join(self, db: Session, model2: Model) -> List[Model]:
        model_obj_all = db.query(self.model, model2).join(model2).all()
        return model_obj_all
